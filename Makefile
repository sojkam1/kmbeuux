all:

install:
	install -D -m644 -t $(DESTDIR)/usr/share/cups/model $(wildcard *.ppd)
	install -D -m755 -t $(DESTDIR)/usr/lib/cups/filter/ KMbeuEmpPS.pl KMbeuEnc.pm
	install -D -m644 -t $(DESTDIR)/usr/share/doc/kmbeuux 'BEU Linux CUPS Driver Guide.pdf'
